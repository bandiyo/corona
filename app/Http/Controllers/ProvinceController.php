<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Province;
use App\Country;
use Response;
class ProvinceController extends Controller
{
    public function home(){
    	return view('frontend.home');
    }
    public function getcountry(){
    	$response = Http::get('https://api.kawalcorona.com/indonesia/');
    	$data = $response->json();

    	foreach ($data as $key) {
    		$array = [
	    		'nama'=>$key['name'],
	    		'positif'=>$key['positif'],
	    		'sembuh'=>$key['sembuh'],
	    		'meninggal'=>$key['meninggal']
    		];
    	}

    	$this->storecountry($array);
    	echo "sukses";

    }

    public function storecountry($array)
    {
      $country = new Country;
      $country->nama = $array['nama'];
      $country->positif = $array['positif'];
      $country->sembuh = $array['sembuh'];
      $country->meninggal = $array['meninggal'];

      $country->save();
    }

    public function getprovince(){
    	$response = Http::get('https://api.kawalcorona.com/indonesia/provinsi');

    	$data =  json_decode($response,true);
    	foreach ($data as $key) {
    		
    		$array = [
	    		'FID'=>$key['attributes']['FID'],
	    		'Kode_Provi'=>$key['attributes']['Kode_Provi'],
	    		'provinsi'=>$key['attributes']['Provinsi'],
	    		'positif'=>$key['attributes']['Kasus_Posi'],
	    		'sembuh'=>$key['attributes']['Kasus_Semb'],
	    		'meninggal'=>$key['attributes']['Kasus_Meni']
    		];

        $this->storeprovince($array);

    	}

    }

    public function storeprovince($array)
    {
        $province = new Province;

        $province->FID = $array['FID'];
        $province->Kode_Provi = $array['Kode_Provi'];
        $province->provinsi = $array['provinsi'];
        $province->positif = $array['positif'];
        $province->sembuh = $array['sembuh'];
        $province->meninggal = $array['meninggal'];

        $province->save();
    }

    public function getlastupdate()
    {
      $response = Http::get('https://api.kawalcorona.com/');
      $data = $response->json();
      if ($data==null)
      {
         return $datatime = "Server Pusat Down";
      }
      for ($i=0; $i <count($data); $i++)
      { 
        if($data[$i]['attributes']['Country_Region'] == 'Indonesia')
        {
          $unixtime = $data[$i]['attributes']['Last_Update'] / 1000;
          $time = date('d-m-Y / H:i:s' , $unixtime);
          date_default_timezone_set('Asia/Jakarta');
          $datatime = $time.' WIB';
        }
      }
      return Response::json($datatime);
      

    }

    public function province(){
    	$data = Province::all();
    	
    	return Response::json($data);
    }

   	public function country(){
   		$data = Country::find(1);

   		return Response::json($data);
   	}

   	public function deletecountry(){
   		
   		DB::table('countries')->truncate();
   		echo "sukses";
   	}	
   	public function deleteprovinces(){
   		
		DB::table('provinces')->truncate();
		echo "sukses";
   	}
}
