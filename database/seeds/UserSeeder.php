<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'corona',
            'email' => 'corona@gmail.com',
            'password' => Hash::make('corona123'),
        ]);
    }
}
