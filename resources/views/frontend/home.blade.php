<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>COVID-19</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('template/vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('template/vendor/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/vendor/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/vendor/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/vendor/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('template/css/theme.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('template/css/custom.css')}}" rel="stylesheet" media="all">

</head>
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="#">
                            <h2 class="custom-fc-grey">COVID-19</h2>
                        </a>
                    </div>
                    
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                
                <div class="header-button-item js-item-menu">
                    
                </div>
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div>
                            <h2 class="custom-fc-grey">COVID-19</h2>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span">Sumber Data :</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="https://api.kawalcorona.com/">https://api.kawalcorona.com/</a>
                                        </li>
                                        
                                    </ul>

                                 
                                </div>
                                <form class="au-form-icon--sm" action="" method="post">
                                    <input class="au-input--w300 au-input--style2" type="text" placeholder="Search for datas &amp; reports...">
                                    <button class="au-btn--submit2" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </form>
                            </div>

                        </div>
                          <div class="col-md-12">
                                   <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span">Last Update :</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="" class="last_update"></a>
                                        </li>
                                        
                                    </ul>

                                 
                                </div>
                                
                            </div>
                            </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

          

            <!-- STATISTIC-->
            <section class="statistic statistic2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--blue shadow-lg">
                                <h2 class="number nama">0</h2>
                                <span class="desc">Negara</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-pin-drop"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--orange shadow-lg">
                                <h2 class="number positif">0</h2>
                                <span class="desc">Positif</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-nature"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--red shadow-lg">
                                <h2 class="number meninggal">0</h2>
                                <span class="desc">Meninggal</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-account-box-mail"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--green shadow-lg">
                                <h2 class="number sembuh">0</h2>
                                <span class="desc">Sembuh</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-face"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END STATISTIC-->

            <!-- STATISTIC CHART-->
            <section class="statistic-chart">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">statistics</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <!-- CHART-->
                            <div class="statistic-chart-1">
                                <h3 class="title-3 m-b-30">chart</h3>
                                <div class="chart-wrap">
                                    <canvas id="widgetChart5"></canvas>
                                </div>
                                <div class="statistic-chart-1-note">
                                    <span class="big">10,368</span>
                                    <span>/ 16220 items sold</span>
                                </div>
                            </div>
                            <!-- END CHART-->
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <!-- TOP CAMPAIGN-->
                            <div class="top-campaign">
                                <h3 class="title-3 m-b-30">top campaigns</h3>
                                <div class="table-responsive">
                                    <table class="table table-top-campaign">
                                        <tbody>
                                            <tr>
                                                <td>1. Australia</td>
                                                <td>$70,261.65</td>
                                            </tr>
                                            <tr>
                                                <td>2. United Kingdom</td>
                                                <td>$46,399.22</td>
                                            </tr>
                                            <tr>
                                                <td>3. Turkey</td>
                                                <td>$35,364.90</td>
                                            </tr>
                                            <tr>
                                                <td>4. Germany</td>
                                                <td>$20,366.96</td>
                                            </tr>
                                            <tr>
                                                <td>5. France</td>
                                                <td>$10,366.96</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END TOP CAMPAIGN-->
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <!-- CHART PERCENT-->
                            <div class="chart-percent-2">
                                <h3 class="title-3 m-b-30">chart by %</h3>
                                <div class="chart-wrap">
                                    <canvas id="percent-chart2"></canvas>
                                    <div id="chartjs-tooltip">
                                        <table></table>
                                    </div>


                                </div>
                                <div class="chart-info">
                                    <div class="chart-note">
                                        <span class="dot dot--blue"></span>
                                        <span>Positif</span>
                                    </div>
                                    <div class="chart-note">
                                        <span class="dot dot--green"></span>
                                        <span>Sembuh</span>
                                    </div>
                                    <div class="chart-note">
                                        <span class="dot dot--red"></span>
                                        <span>Meninggal</span>
                                    </div>
                                </div>
                            </div>
                            <!-- END CHART PERCENT-->
                        </div>
                    </div>
                </div>
            </section>
            <!-- END STATISTIC CHART-->

            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">data provinsi</h3>
                            
                            <div class="table-responsive table-responsive-data2 shadow">
                                <table class="table table-hover bg-custom-white">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                              
                                            <th>Provinsi</th>
                                            <th>Positif</th>
                                            <th>Meninggal</th>
                                            <th>Sembuh</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody id="tableBody">
                                        <tbody>
                                            <tr id="masterTable" style="display: none">
                                            <td class="no"></td>
                                            
                                            <td class="prov"></td>
                                            <td>
                                                <span class="block-email posit custom-bold"></span>
                                            </td>
                                            <td><span class="status--denied block-email menggl custom-bold">Denied</span></td>
                                            
                                            <td>
                                                <span class="status--process block-email semb custom-bold">Processed</span>
                                            </td>
                                            
                                        
                                        </tr>
                                        
                                        </tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->

            <!-- COPYRIGHT-->
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="{{ asset('template/vendor/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{ asset('template/vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{ asset('template/vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor JS       -->
    <script src="{{ asset('template/vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{ asset('template/vendor/wow/wow.min.js')}}"></script>
    <script src="{{ asset('template/vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{ asset('template/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{ asset('template/vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('template/vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{ asset('template/vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{ asset('template/vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{ asset('template/vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{ asset('template/vendor/select2/select2.min.js')}}">
    </script>

    <!-- Main JS-->
    <script src="{{ asset('template/js/main.js')}}"></script>
    <script src="{{ asset('template/js/jquery.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(e){

            deleteAll();
            function deleteAll(){
               deletecountry(); 
               deleteprovinces();
            }
            
            function deletecountry(){
                $.ajax({
                    method:"GET",
                    url:"{{url('province/deletecountry')}}",
                    success:function(data){
                        if(data=='sukses'){
                            alert('delete data country dan load new data');
                        }else{
                            alert('gagal');
                        }
                    }
                })
            }

             
            function deleteprovinces(){
                $.ajax({
                    method:"GET",
                    url:"{{url('province/deleteprovinces')}}",
                    success:function(data){
                        if(data=='sukses'){
                            alert('delete data provinsi dan load new data');
                        }else{
                            alert('gagal');
                        }
                    }
                })
            }


            getlastupdate()
             function getlastupdate(){
            $.ajax({
                    method:"GET",
                    url:"{{url('province/getlastupdate')}}",
                    success:function(data){
                       
                        $('.last_update').html(data);
                    }
                   
            });
           }

            getcountry()
             function getcountry(){
            $.ajax({
                    method:"GET",
                    url:"{{url('province/getcountry')}}",
                   
            });
           }

             getprovince()
             function getprovince(){
            $.ajax({
                    method:"GET",
                    url:"{{url('province/getprovince')}}",
                   
            });
           }


           country();
           function country(){
            $.ajax({
                    method:"GET",
                    url:"{{url('province/country')}}",
                    success:function(data){
                        $('.nama').html(data.nama);
                        $('.positif').html(data.positif);
                        $('.sembuh').html(data.sembuh);
                        $('.meninggal').html(data.meninggal);
                    }
            });
           }
            province();
            function province(){
                $.ajax({
                    method:"GET",
                    url:"{{url('province/province')}}",
                    success:function(data){
                        $.each(data,function(i,row){
                            

                            obj = $('#masterTable').clone();
                            obj.show();

                            obj.find('.no').html(i+1);
                            obj.find('.prov').html(row.provinsi);
                            obj.find('.posit').html(row.positif);
                            obj.find('.menggl').html(row.meninggal);
                            obj.find('.semb').html(row.sembuh);

                            $('#tableBody').append(obj);

                        })
                    }
                 });
            }


            
        });
    </script>

</body>

</html>
<!-- end document-->
