<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('province')->group(function () {
	Route::get('/', 'ProvinceController@home');
	Route::get('/country', 'ProvinceController@country');
	Route::get('/province', 'ProvinceController@province');
	Route::get('/getcountry', 'ProvinceController@getcountry');
	Route::get('/getprovince', 'ProvinceController@getprovince');
	Route::get('/getlastupdate', 'ProvinceController@getlastupdate');
	Route::get('/deletecountry', 'ProvinceController@deletecountry');
	Route::get('/deleteprovinces', 'ProvinceController@deleteprovinces');
	
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

